import React, { useState, useEffect, useRef } from "react";
import { Checkbox as CheckBox } from "@material-ui/core";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Button } from "@material-ui/core";

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  cursor: pointer;
  padding: 4px 15px;
  background-color: ${({ focus }) => (focus ? "#f2fcfe" : "#ffffff")};
`;
const Text = styled.span`
  color: #7f7f7f;
  font-size: 15px;
  margin-left: 7px;
`;
const StyledCheckbox = styled(CheckBox)`
  && {
    padding: 0;
    color: ${({ color }) => color};
  }
`;
const StyledButton = styled(Button)`
  && {
    padding: 0;
    font-size: 10px;
    color: #80b1d2;
  }
`;
const CheckedBox = styled.div`
  width: 18px;
  height: 18px;
  margin: 3px;
  border-radius: 3px;
  background-color: ${({ color }) => color};
  background-image: url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath 
    fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003
    1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\");
`;

export const Checkbox = ({
  index,
  text,
  color = "#2097f5",
  checked = false,
  onCheck,
  onlyThis
}) => {
  const containerRef = useRef(null);
  const [focusHere, setFocusHere] = useState(false);
  const refsClean = () => {
    containerRef.current.removeEventListener("mouseover", () => { });
    containerRef.current.removeEventListener("mouseout", () => {});
  };
  useEffect(() => {
    containerRef.current.addEventListener("mouseover", () =>
      setFocusHere(true)
    );
    containerRef.current.addEventListener(
      "mouseout",
      e => !containerRef.current.contains(e.toElement) && setFocusHere(false)
    );
    return () => refsClean();
  }, [containerRef]);
  return (
    <Container
      focus={focusHere}
      ref={containerRef}
      onClick={() => onCheck(index)}
    >
      <div>
        <StyledCheckbox
          checked={checked}
          checkedIcon={<CheckedBox color={color} />}
        ></StyledCheckbox>
        <Text>{text}</Text>
      </div>
      {focusHere && (
        <StyledButton
          onClick={e => {
            onlyThis(index);
            e.stopPropagation();
          }}
        >
          Только
        </StyledButton>
      )}
    </Container>
  );
};

Checkbox.propTypes = {
  index: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  onCheck: PropTypes.func.isRequired,
  onlyThis: PropTypes.func.isRequired
};
