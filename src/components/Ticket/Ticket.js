import React from "react";
import styled from "styled-components";
import { Button, Divider } from "@material-ui/core";
import transferAirplane from "../../images/air.svg";
import PropTypes from "prop-types";
import moment from "moment";
import "moment/locale/ru";
const Container = styled.div`
  display: flex;
  background-color: #ffffff;
  height: 145px;
  border-radius: 5px;
  margin: 0 0 20px 0;
  width: 690px;
`;
const LeftSide = styled.div`
  max-width: 180px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  margin: 20px;
`;
const CompanyName = styled.span`
  font-size: 15px;
  text-align: center;
`;
const BuyButton = styled(Button)`
  && {
    padding: 6px 30px;
    font-size: 17px;
    line-height: 1.4;
    text-transform: none;
    background-color: #ff6c00;
    color: #ffffff;
    &:hover {
      opacity: 0.85;
      background-color: #ff6c00;
    }
  }
`;
const DepartureContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 15px 0 0 20px;
`;
const ArrivalContainer = styled(DepartureContainer)`
  margin: 15px 20px 0 0;
`;
const TransferInformation = styled(DepartureContainer)`
  margin: 15px 20px 0 0;
`;
const Time = styled.span`
  font-size: 34px;
`;
const Origin = styled.span`
  font-size: 14px;
`;
const Destination = styled(Origin)``;
const Date = styled.span`
  font-size: 14px;
  opacity: 0.5;
`;
const TransferCount = styled.span`
  font-size: 14px;
  opacity: 0.4;
  padding: 0 20px 5px;
  border-bottom: solid 2px;
`;
const TransferAirplane = styled.div`
  transform: rotate(90deg);
  position: absolute;
  margin-left: 103px;
  margin-top: 11px;
  width: 24px;
  height: 24px;
  background: url(${transferAirplane});
`;

moment.localeData("ru");

export const Ticket = ({
  origin,
  origin_name,
  destination,
  destination_name,
  departure_date,
  departure_time,
  arrival_date,
  arrival_time,
  carrier,
  stops,
  price,
  onBuy,
  index
}) => {
  const departureDate = moment(departure_date, "DD.MM.YYYY").format(
    "Do MMMM YYYY, dd"
  );
  const arrivalDate = moment(arrival_date, "DD.MM.YYYY").format(
    "Do MMMM YYYY, dd"
  );
  const getTransferCount = stops => {
    return `${stops > 0 ? stops : "Без"} ${
      stops === 1
        ? "пересадка"
        : stops > 4 || stops === 0
        ? "пересадок"
        : "пересадки"
    }`;
  };
  return (
    <Container>
      <LeftSide>
        <CompanyName>Эмблема компании перевозчика: {carrier}</CompanyName>
        <BuyButton variant="contained" onClick={() => onBuy(index)}>
          Купить за {price}₽
        </BuyButton>
      </LeftSide>
      <Divider orientation="vertical" variant="middle" />
      <DepartureContainer>
        <Time>{departure_time}</Time>
        <Origin>
          {origin}, {origin_name}
        </Origin>
        <Date>{departureDate}</Date>
      </DepartureContainer>
      <TransferInformation>
        <TransferCount>{getTransferCount(stops)}</TransferCount>
        <TransferAirplane />
      </TransferInformation>
      <ArrivalContainer>
        <Time>{arrival_time}</Time>
        <Destination>
          {destination}, {destination_name}
        </Destination>
        <Date>{arrivalDate}</Date>
      </ArrivalContainer>
    </Container>
  );
};

Ticket.propTypes = {
  origin: PropTypes.string.isRequired,
  origin_name: PropTypes.string.isRequired,
  destination: PropTypes.string.isRequired,
  destination_name: PropTypes.string.isRequired,
  departure_date: PropTypes.string.isRequired,
  departure_time: PropTypes.string.isRequired,
  arrival_date: PropTypes.string.isRequired,
  arrival_time: PropTypes.string.isRequired,
  carrier: PropTypes.string.isRequired,
  stops: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  onBuy: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired
};
