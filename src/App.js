import React, { useState } from "react";
import styled from "styled-components";
import "typeface-roboto";
import airplaneImg from "./images/airplane.png";
import { Button, ButtonGroup } from "@material-ui/core";
import { Checkbox } from "./components/Checkbox";
import { Ticket } from "./components/Ticket";

const Container = styled.div`
  position: absolute;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #f3f7fa;
  min-height: 100%;
  height: fit-content;
`;
const AirImage = styled.div`
  height: 100px;
  width: 100px;
  margin-top: 50px;
  background: url(${airplaneImg});
  background-repeat: no-repeat;
`;
const InformationContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 50px;
  width: inherit;
  min-width: 940px;
  max-width: 970px;
`;
const FilterContainer = styled.div`
  background-color: #ffffff;
  width: 240px;
  border-radius: 5px;
  height: fit-content;
`;
const CurrencyContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 15px;
`;
const CurrencyText = styled.span`
  color: #7f7f7f;
  font-size: 15px;
  text-transform: uppercase;
`;
const TransferText = styled(CurrencyText)`
  margin-left: 15px;
`;
const StyledButton = styled(Button)`
&& {
  color: ${({ variant }) => (variant === "outlined" ? "#2097f5" : "#ffffff")}
  background-color: ${({ variant }) =>
    variant === "outlined" ? "#ffffff" : "#2097f5"}
  box-shadow: none;
  padding: 10px 20px;
  ${({ variant }) =>
    variant === "outlined"
      ? `&:hover {
    box-shadow: none;
    background-color: #f2fcfe;
    border-color: #2097f5;
    color: #80b1d2;
  }`
      : `&:hover {
    box-shadow: none;
    background-color: #2097f5;
    border-color: #2097f5;
    color: #ffffff;
  }`}
}  
`;
const StyledButtonGroup = styled(ButtonGroup)`
  && {
    margin-top: 10px;
  }
`;
const TransferContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 15px 0;
`;
const CheckboxContainer = styled(TransferContainer)`
  justify-content: space-between;
  margin-top: 10px;
  height: 150px;
`;
const TicketsContainer = styled(TransferContainer)`
  margin: 0;
`;

const transferMock = [
  { text: "Все", selected: true, expression: e => true },
  { text: "Без пересадок", selected: false, expression: e => e === 0 },
  { text: "1 пересадка", selected: false, expression: e => e === 1 },
  { text: "2 пересадки", selected: false, expression: e => e === 2 },
  { text: "3 пересадки", selected: false, expression: e => e === 3 }
];
const dataMock = {
  tickets: [
    {
      id: "1",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "16:20",
      arrival_date: "12.05.18",
      arrival_time: "22:10",
      carrier: "TK",
      stops: 3,
      price: 12400
    },
    {
      id: "2",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "17:20",
      arrival_date: "12.05.18",
      arrival_time: "23:50",
      carrier: "S7",
      stops: 1,
      price: 13100
    },
    {
      id: "3",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "12:10",
      arrival_date: "12.05.18",
      arrival_time: "18:10",
      carrier: "SU",
      stops: 0,
      price: 15300
    },
    {
      id: "4",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "17:00",
      arrival_date: "12.05.18",
      arrival_time: "23:30",
      carrier: "TK",
      stops: 2,
      price: 11000
    },
    {
      id: "5",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "12:10",
      arrival_date: "12.05.18",
      arrival_time: "20:15",
      carrier: "BA",
      stops: 3,
      price: 13400
    },
    {
      id: "6",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "9:40",
      arrival_date: "12.05.18",
      arrival_time: "19:25",
      carrier: "SU",
      stops: 3,
      price: 12450
    },
    {
      id: "7",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "17:10",
      arrival_date: "12.05.18",
      arrival_time: "23:45",
      carrier: "TK",
      stops: 1,
      price: 13600
    },
    {
      id: "8",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "6:10",
      arrival_date: "12.05.18",
      arrival_time: "15:25",
      carrier: "TK",
      stops: 0,
      price: 14250
    },
    {
      id: "9",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "16:50",
      arrival_date: "12.05.18",
      arrival_time: "23:35",
      carrier: "SU",
      stops: 1,
      price: 16700
    },
    {
      id: "10",
      origin: "VVO",
      origin_name: "Владивосток",
      destination: "TLV",
      destination_name: "Тель-Авив",
      departure_date: "12.05.18",
      departure_time: "6:10",
      arrival_date: "12.05.18",
      arrival_time: "16:15",
      carrier: "S7",
      stops: 0,
      price: 17400
    }
  ]
};
dataMock.tickets.sort((prev, next) => prev.price - next.price);
const currencys = ["RUB", "USD", "EUR"];
const App = () => {
  const [currensySelected, setCurrencySelected] = useState("RUB");
  const [transfers, setTransfers] = useState(transferMock);
  const [tickets, setTickets] = useState(dataMock); // setTickets left for scalability
  const buyTicket = index => {
    alert(`Вы выбрали билет №${index}`);
  };
  const handleCurrency = e => {
    setCurrencySelected(e.currentTarget.value);
  };
  return (
    <Container>
      <AirImage />
      <InformationContainer>
        <FilterContainer>
          <CurrencyContainer>
            <CurrencyText>Валюта</CurrencyText>
            <StyledButtonGroup size="small">
              {currencys.map(data => (
                <StyledButton
                  key={data}
                  variant={currensySelected === data ? "contained" : "outlined"}
                  value={data}
                  onClick={handleCurrency}
                >
                  {data}
                </StyledButton>
              ))}
            </StyledButtonGroup>
          </CurrencyContainer>
          <TransferContainer>
            <TransferText>Количество пересадок</TransferText>
            <CheckboxContainer>
              {transfers.map((data, index) => (
                <Checkbox
                  key={data.text}
                  index={index}
                  checked={data.selected}
                  text={data.text}
                  onCheck={index =>
                    setTransfers(
                      transfers.map(
                        (data, innerIndex) =>
                          innerIndex === index ? { ...data, selected: !data.selected } : data
                      )
                    )
                  }
                  onlyThis={index =>
                    setTransfers(
                      transfers.map((data, innerIndex) =>
                        innerIndex === index
                          ? { ...data, selected: true }
                          : { ...data, selected: false }
                      )
                    )
                  }
                />
              ))}
            </CheckboxContainer>
          </TransferContainer>
        </FilterContainer>
        <TicketsContainer>
          {tickets.tickets
            .filter(data => {
              const filtersArray = transfers.map(
                (transfer) =>
                  transfer.selected && transfer.expression(data.stops)
              );
              return filtersArray.includes(true) ? data : false;
            })
            .map((ticket, index) => (
              <Ticket
                key={ticket.id}
                index={index}
                onBuy={e => buyTicket(e)}
                {...ticket}
              ></Ticket>
            ))}
        </TicketsContainer>
      </InformationContainer>
    </Container>
  );
};

export default App;
